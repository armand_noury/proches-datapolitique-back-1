https://www.data.gouv.fr/ (gouv)
  Les Sénateurs +
  Amendements déposés au Sénat
  Amendements XIV ième législature (en fait concerne la XV et est le fichier fourni par l'an) -> idem fourni par an
https://data.senat.fr/
  les-senateurs https://data.senat.fr/data/senateurs/export_sens.zip
  donnees/ (sen)  
    Les travaux législatifs (Dosleg) https://data.senat.fr/data/dosleg/dosleg.zip
    Les amendements (Ameli) https://data.senat.fr/data/ameli/ameli.zip
    Les questions (Basile-questions) https://data.senat.fr/data/questions/questions.zip
    Les comptes rendus des débats https://data.senat.fr/data/debats/debats.zip
  https://data.assemblee-nationale.fr/ (an)
  Acteurs (députés)
    Députés en exercice 
      composite https://data.assemblee-nationale.fr/static/openData/repository/15/amo/deputes_actifs_mandats_actifs_organes/AMO10_deputes_actifs_mandats_actifs_organes_XV.json.zip
      acteurs et organes separés https://data.assemblee-nationale.fr/static/openData/repository/15/amo/deputes_actifs_mandats_actifs_organes_divises/AMO40_deputes_actifs_mandats_actifs_organes_divises_XV.json.zip
    Historique des députés https://data.assemblee-nationale.fr/static/openData/repository/15/amo/tous_acteurs_mandats_organes_xi_legislature/AMO30_tous_acteurs_tous_mandats_tous_organes_historique.json.zip
    Députés-sénateurs-ministres https://data.assemblee-nationale.fr/static/openData/repository/15/amo/deputes_senateurs_ministres_legislature/AMO20_dep_sen_min_tous_mandats_et_organes_XV.json.zip
    Députés nommés dans un OEP https://data.assemblee-nationale.fr/static/openData/repository/15/amo/oep_csv_opendata/liste_organismes_extra_parlementaires_libre_office.csv
    Liste simplifiée des mandats https://data.assemblee-nationale.fr/static/openData/repository/15/amo/mandats_csv_opendata/mandats_ge_ga_gevi_libre_office.csv
  Travaux parlementaires
    Dossiers législatifs +
    Séance publique
    Amendements +
    Débats
    Votes
    Questions
      Questions au gouvernement
      Questions orales sans débat
      Question écrites
  Réunions
  OpenData archives XIVe
    Députés, sénateurs et ministres XIVe législature
    Amendements XIVe législature
    Dossiers législatifs XIVe législature
    Scrutins XIVe législature
    Questions écrites XIVe législature
    Questions au Gouvernement XIVe législature
    Questions orales sans débat XIVe législature
    Agendas XIVe législature