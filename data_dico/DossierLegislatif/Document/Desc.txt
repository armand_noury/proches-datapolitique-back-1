XV ième législature
Source url : https://data.assemblee-nationale.fr/static/openData/repository/15/loi/dossiers_legislatifs/Dossiers_Legislatifs_XV.json.zip
Source file : Dossiers_Legislatifs_XV.json.zip
Source expanded : json dir (json/dossierParlementaire & json/document/)
Source expanded path : data_sources/an/dosleg/Dossiers_Legislatifs_XV/json/document/ 

Fichiers
DLRXXX = dossier legilatif
PIONAXXX = texte legislatif (proposition de loi)
ACINAXXX = texte legislatif accord international (proposition de loi)
AVCEANXXX = texte legislatif avis conseil d'état (proposition de loi)
AVISAN = avis provenance commision (an pour assemblée nationale ?)
AVISSN = avis provenance commision (sn pour sénat ?)

XIV ième législature
Pas trouvé
