#!/usr/bin/env bash
abs_path=$(pwd)
if [ "$1" == "" ]; then echo "Il manque le premier argument"; exit 1; fi
if [ -f "$1" ]; then 
  f_dest=$1"-formatted"
  cat $1 | python -mjson.tool > $f_dest;
  mv $f_dest $1
  exit 1; 
fi
dir_path=$1
if [ ! -d "$1" ]; then echo "Le répertoire $1 n'existe pas"; exit 1; fi
if [ -d "$dir_path/json" ]; then dir_path="$dir_path/json"; fi
jsons_list="$(find $dir_path -name "*.json")"
num_files=$(echo "$jsons_list" |wc -l)
echo "Nombre de fichiers : $num_files"
cpt=1
for f in ${jsons_list[@]}
do
  echo -ne "\r$cpt : $f"
  f_content="$(cat $f)"
  # si le fichier doit être formaté (il ne contient aucun saut de ligne)
  if [[ "$(echo "$f_content" |wc -l| rev | cut -d" " -f1 | rev)" == 1 ]]; then
    f_dest=$f"-formatted"
    echo "$f_content" | python -mjson.tool > $f_dest;
    mv $f_dest $f
  fi
  ((cpt ++));
done