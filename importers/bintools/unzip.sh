#!/usr/bin/env bash
abs_path=$(pwd)
if [ "$1" == "" ]; then echo "Il manque le premier argument"; exit 1; fi
if [ ! -f "$1" ]; then echo "Le fichier $1 n'existe pas"; exit 1; fi

dir_dest=${1//.json.zip/}
dir_dest=${dir_dest//.zip/}
if [ ! -d "$dir_dest" ]; then mkdir $dir_dest; fi

unzip $1 -d $dir_dest