<?php
Namespace Datapol\Bootstrap;
ini_set("display_errors", "stderr");
error_reporting(E_ALL);
//ini_set("display_errors", true);
ini_set("memory_limit", "10G");

require(dirname(__FILE__)."/../../vendor/autoload.php");
use Symfony\Component\Yaml\Yaml;

Class Parameters {
  public $params;
  public $configs;

  function __construct() {
    $env = file_get_contents(dirname(__FILE__)."/../../../.env");
    foreach (explode("\n", $env) as $line) {
      $varVal = explode("=", $line);
      putenv($varVal[0]."=".$varVal[1]);
    }
    $configsYml = file_get_contents(dirname(__FILE__)."/../../configs/data-collection.yml");
    $this->configs = Yaml::parse($configsYml);
    $paramsYml = file_get_contents(dirname(__FILE__)."/../../configs/params.yml");
    $this->params = Yaml::parse($paramsYml);
  }
}