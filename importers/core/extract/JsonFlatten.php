<?php

namespace DataPol\Extract;
Class JsonFlatten {
  public $values = array();
  // Formate les noms de colonne extrapolées des nodes json
  static function sanitizeColumnName($name) {
    $name = str_replace("@", "_", $name);
    $name = str_replace(":", "_", $name);
    $name = substr($name, 0, 10);

    return $name;
  }

  /* 
    Parourt un tableau associatif dans toute sa profondeur et l'applatit au format :
    [parentkey1-parentkey2-parentkeyN] = valeur
  */
  function recurse($parent, $array){
    foreach ($array as $k=>$val){
      if (is_array($val)) {
        $this->recurse($parent."-".$k,$val);
      } else {
        $this->values[$parent."-".$k] = $val;
      }
    }
  }

  static function nodePath2ArrayPath($nodePath){
      $keysArray = explode("->", $nodePath);
      foreach ($keysArray as $key){
        $arrayKey[] = "['".$key."']";
      }
      $arrayKey = implode("",$arrayKey);

      return $arrayKey;
  }
}

