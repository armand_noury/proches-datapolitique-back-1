<?php

namespace DataPol\Extract;
Class JsonSplit {
  public $jsonFilePath;
  public $jsonExportDirPath;
  public $data;
  public $parentKey;
  public $callBackFileNameKey;

  function __construct($jsonFilePath, $jsonExportDirPath) {
      $this->jsonFilePath = $jsonFilePath;
      $this->jsonExportDirPath = $jsonExportDirPath;
      if ( ! file_exists($jsonExportDirPath) ) {
        mkdir($jsonExportDirPath);
      }
      $this->data = json_decode(file_get_contents($jsonFilePath));
  }
  function split(){
    foreach($this->parentKey as $k => $v) {
      $spliFileName = ($this->callBackFileNameKey)($v);
      file_put_contents($this->jsonExportDirPath."/".$spliFileName, json_encode($v));
    }
  }
}

class JsonSplitItem {

  function __construct(){

  }

}

